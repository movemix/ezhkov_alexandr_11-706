﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 40;
            int y = 50;
            x = x + y; // выражение равно 90
            y = x - y; // выражение равно 40
            x = x - y; // выражение равно 50 

            Console.WriteLine(x);// 50
            Console.WriteLine(y);// 40
        }
    }
}
