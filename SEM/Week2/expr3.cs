﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            var H = 8;
            var x = 360 / 12; //1 час = 360 / 12 = 30° 
            var CornerH = x * 4; // 4 это количество часов от 8ми до 12ти, т.е. 4 угла по 30 градусов.
            Console.WriteLine(CornerH); // наш угол = 120 градусов
        }
    }
}
