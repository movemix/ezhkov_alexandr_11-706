﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 526; // задаем переменную равную 526
            string b = a.ToString(); // переводим число в строку
            char[] arr = b.ToCharArray(); // переводим строку в массив
            Array.Reverse(arr); // Используем Reverse для перобразования в [6][2][5]
            string s = new String(arr); // преобразуем обратно в строку
            a = Convert.ToInt32(s); // преобразовываем в int
            Console.WriteLine(a); // 625
        }
    }
}
