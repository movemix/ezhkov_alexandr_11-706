﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp16
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = Console.ReadLine().Length;
            var sum = 0;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < 2 * i; j++)
                    sum++;
            Console.WriteLine(sum);
        }
    }
}
