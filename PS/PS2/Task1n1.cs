﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp114
{
    class Program
    {
        static void Main(string[] args)
        {
            double e = double.Parse(Console.ReadLine());
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Значение последовательности - {0}" ,CalculateNumInDegree(e, x));
            Console.ReadKey();
        }

        public static double CalculateNumInDegree(double e, double x)
        {
            int step = 1;
            double k = 1;
            double summ = 0;
            while (k > e)
            {
                summ += k;
                step++;
                k *= (x / (step - 1));                                
            }
            Console.WriteLine("точность достигнута на шаге - {0}", step);
            return summ;
        }
    }
}
