﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp66
{
    class Program
    {
        static void Main(string[] args)
        {
            double TWO(double b, int c)
            {
                double p;
                if (c < 0)
                {
                    p = 1 / b;
                    c = -c;
                    for (int i = 1; i < c; i++)
                        p = p / b;
                }
                else
                {
                    p = b;
                    for (int i = 1; i < c; i++)
                        p = p * b;
                }
                return p;
            }
        }
    }
}
