﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp65
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Сумма чисел: {0}",res());
            Console.Read();
        }
        public static int res()
        {
            int sum = 0;
            for (int i = 0; i < 1000; i++)
            {
                if ((i % 3 == 0) || (i % 5 == 0))
                {
                    sum += i;
                    Console.WriteLine(i);
                    Console.ReadKey();
                }
            }
            return sum;
        }
    }
}
