﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp68
{
    class Program
    {
        static void Main(string[] args)
        {
                int n = int.Parse(Console.ReadLine()); //вводим цифру в консоль
                int factorial = 1; //значение факториала

                for(int i = 2; i<=n; i++)
                {
                    factorial = factorial * i;   
                }

                Console.WriteLine(factorial % 10);
                Console.ReadKey();

        }
    }
}
